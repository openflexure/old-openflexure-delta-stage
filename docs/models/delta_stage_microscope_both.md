# Delta Stage microscope main body

[Delta Stage Microscope]: delta_stage_microscope.stl
[Delta Stage Microscope smart brim]: delta_stage_microscope_smart_brim.stl

This is the body for the complete microscope, with the reflection illumination hole and transmission illumination condenser mount.

> **Warning:** You should not print the Delta Stage Microscope main body using a brim as it will interfere with the mechanism.  Instead use the smart brim version.

## Delta Stage Microscope

[Delta Stage Microscope]

## Delta Stage Microscope smart brim

[Delta Stage Microscope smart brim]